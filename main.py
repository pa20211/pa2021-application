import boto3
import json

from nlp import *

from pprint import pprint

import re
import numpy as np
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import linear_kernel
from nltk.corpus import stopwords



sqs_client = boto3.client("sqs", region_name="us-east-2")
url = "https://sqs.us-east-2.amazonaws.com/344733421702/processApplication"


def receive_message():
    response = sqs_client.receive_message(
        QueueUrl=url,
        MaxNumberOfMessages=1,
        WaitTimeSeconds=10,
    )


    print(f"Number of messages received: {len(response.get('Messages', []))}")

    for message in response.get("Messages", []):
        message_body = message["Body"]
        print(f"Message body: {json.loads(message_body)}")
        print(f"Receipt Handle: {message['ReceiptHandle']}")

def delete_message(receipt_handle):
    response = sqs_client.delete_message(
        QueueUrl = url,
        ReceiptHandle = receipt_handle,
    )
    print(response)



def get_input():
    file = open("application.json")

    data = json.load(file)

    with SimilarityMatcher(data) as matcher:
        matcher.output()


get_input()
