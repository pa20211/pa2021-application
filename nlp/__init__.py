import re
import numpy as np
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import linear_kernel
from nltk.corpus import stopwords


class SimilarityMatcher:
    def __init__(self, application, threshold = .7):
        applicant = application['applicant']
        offer = application['offer']
        self.vectorizer = TfidfVectorizer(stop_words = stopwords.words('french'))
        self.text = [' '.join(
                [education['description'] for education in applicant['profile']['educations']] +
                [experience['description'] + " " + experience["jobTitle"] for experience in applicant['profile']['experiences']] +
                [skill['name'] for skill in applicant['profile']['skills']] +
                [certification['certificationName'] for certification in applicant['profile']['certifications']]) + applicant['profile']['profileTitle'],
               offer['summaryOfNeed'] + " " +  offer['expectedCompetencies'] + " " + offer['descriptionOffer']]
        self.offer = offer
        self.applicant = applicant
        self.threshold = threshold
        self.application_id = application['id']

    def __enter__(self):
        return self


    def __exit__(self, exc_type, exc_value, traceback):
        pass

    def pre_validate(self):
        applicant = self.applicant
        offer = self.offer
        return applicant['availabilityDate'] <= offer['jobStartDate'] and \
            applicant['tjExpected'] <= offer['tjExpected'] and \
            (( applicant['country'] != offer['employer']['country'] and \
            applicant['geographicMobility'] != 'international') or \
            applicant['country'] == offer['employer']['country'] ) and \
            applicant['minimumExpectedMissionDuration'] <= offer['missionDuration']

    def preprocessing(self, text):
        text = text.lower() # mettre les mots en minuscule

        return re.sub(r"[\W\d_]+", " ", text,flags= re.UNICODE)
        #print(text)
        #text = re.sub(r"\s\s+", " ", text)
        #return text

    def get_cosine_sim(self):
        corpus = [self.preprocessing(info) for info in self.text]
        print(corpus)
        tfidf_matrix = self.vectorizer.fit_transform(corpus)

        return linear_kernel(tfidf_matrix, tfidf_matrix)


    def nlp_valid(self):
        if self.pre_validate() :
            cosine_sim = self.get_cosine_sim()
            print(cosine_sim)
            return cosine_sim[0][1] > self.threshold
        return False

    def output(self):
        js = f'''{{
            "dateChanged": "{{{{dateCreation}}}}",
            "applicationstatus": {{
                "status": "{"ACCEPTED" if self.nlp_valid() else "DENIED"}"
            }},
            "application": {{
                "id": {self.application_id}
            }}
        }}'''
        print(js)

